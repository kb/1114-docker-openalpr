Automatic License Plate Recognition
***********************************

https://github.com/openalpr/openalpr

Build docker image::

  docker build -t openalpr https://github.com/openalpr/openalpr.git

Download test image::

  wget http://plates.openalpr.com/h786poj.jpg

Run alpr on image::

  docker run -it --rm -v $(pwd):/data:ro openalpr -c eu h786poj.jpg

For video, checkout:
https://groups.google.com/forum/#!searchin/openalpr/video/openalpr/7FhH4Uefj9Y/P_9O4bS0b0wJ
